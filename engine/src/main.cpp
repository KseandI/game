#include "main.hpp"

int
main() {
  Engine::Engine eng;
  while (eng.loop()) {};
  return 0;
};
