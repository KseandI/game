#ifndef ENGINE_INCLUDE_FILE
#define ENGINE_INCLUDE_FILE
#define GLEW_STATIC
#include <GL/glew.h>
#include <vector>
#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <iostream>

namespace Engine {
  class Engine {
  private:
    SDL_Window* m_window = nullptr;
    SDL_GLContext m_glContext;
    SDL_Event* m_event = nullptr;
    int m_height = 600;
    int m_width = 800;
  public:
    Engine();
    ~Engine();
    int loop();
  };
};

int main();

#endif // ENGINE_INCLUDE_FILE
